Ansible Role: Php-fpm
===================
Install and configure PHP-fpm on Debian servers

It Install php-fpm packages and its related packages, will configure all requested php-fpm pools. It will also install database driver (`mysql` or `pgsql`) for php

----------
Requirement
--------------

If `php_socket_dir` is in a temporary mounted file system e.g: `php_socket_dir: /var/run/problematic_folder`, you need to ensure that the folder will exist after each reboot.

By default it is set to `var/run` and this not create any problem.


Role variables
-------------
Available variables are listed below, along with default values (see `defaults/main.yml`):

	php_fpm_user: www-data
	php_fpm_group: www-data
	php_fpm_listen_owner: www-data
	php_fpm_listen_group: www-data
These variables represent global defaults and are used if variable are not defined inside each single pool.

	php_socket_dir:  "/var/run/"
the socket directory where each pool socket will reside (see *Requirement*)

	php_database_driver: ""
The database driver the role has to install

	php_fpm_pools: []
A list of php pools handled separately by php-fpm. Each pool has these variable with the respective defaults:
	
	- id: <n/a>
	  socket: pool.sock
	  user: {{php_fpm_user}}
	  group: {{php_fpm_group}}
	  listen_owner: {{php_fpm_listen_owner}}
	  listen_group: {{php_fpm_listen_group}}
	  log_dir:  <n/a>
	  access_log: access.log
	  error_log: error.log
	  pm: dynamic # can be [ dynamic | ondemand | static] 
	  pm_max_children: 5
	  pm_start_servers: 2        # if pm=dynamic
	  pm_min_spare_servers: 1    # if pm=dynamic
	  pm_max_spare_servers: 3    # if pm=dynamic
	  pm_max_requests: <n/a>
	  pm_process_idle_timeout: 10s   # if pm=ondemand
	
where  `user`/`group` is the owner of the pools and defaults to the global. `listen_owner`/`listen_group` is the the owner of the socket and **should** be accessible by webserver (a good value is the owner of the webserver `www-data` in a Debian env). `pm` is the process management and could be `dynamic` | `ondemand` | `static`. If `dynamic` is used you can specify `_max`, `_min_spare_servers` and `max_spare_servers`. Otherwise if `ondemand` is specified you can set `pm_process_idle_timeout`
   
Dependencies
-------------------
None.

Example Playbook
--------------------------

    - name: deploy php
      hosts: webservers
      roles:
        - role: php-fpm
          php_database_driver: pgsql
          php_fpm_pools:
            - id: site1.example.com
              socket: site1.sock
              user: site1_user
              group: site1_group
              log_dir: /var/www/site1.example.com/phplog
            - id: site2.example.com
              socket: site2.sock
              user: site2_user
              group: site2_group
              log_dir: /var/www/site2.example.com/phplog
 

TODO
-------------
* Manage `php.ini` config file